﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace HubClubBot
{
    public class MyCommands
    {
        [Command("hi")]
        public async Task Hi(CommandContext ctx)
        {
            await ctx.RespondAsync($"Hello, Mr. {ctx.User.Mention}.");
        }

        [Command("menu")]
        public async Task menu(CommandContext ctx)
        {
            await ctx.RespondAsync(
                $"Mr. {ctx.User.Mention}, your menu: ```straight - the finest of women, occasionally with an auxiliary male for a little extra kick.\ngay - the most gorgeous men. ask about the manager's special.\ndickgirl - the feminine form, with a masculine endowment.\ntf - tales of alteration and change.\ncute - our safe for work section, for when one might need respite from the smuttery.```");
        }
        
        [Command("dat")]
        public async Task images(CommandContext ctx, string flavor, int howMany = 1)
        {
            Channel channel;

            switch (flavor.ToLower())
            {
                case "straight":
                    channel = Channel.Straight;
                    break;
                case "gay":
                    channel = Channel.Gay;
                    break;
                case "cute":
                    channel = Channel.Cute;
                    break;
                case "tf":
                    channel = Channel.Tf;
                    break;
                case "dickgirl":
                    channel = Channel.Dickgirl;
                    break;
                case "ugay":
                    channel = Channel.UGay;
                    break;
                case "ustraight":
                    channel = Channel.UStraight;
                    break;
                default:
                    await ctx.RespondAsync($"Mmm, I'm dreadfully sorry, but we don't serve {flavor} at this establishment. If you would like a menu, simply and ask me to `!gib menu`.");
                    return;
            }

            Console.WriteLine("Order built.");

            if (howMany > 5)
                howMany = 5;

            string response = await BuildReply((ulong)channel, ctx, howMany);

            Console.WriteLine($"serving {howMany} {flavor} pictures for {ctx.User.Username}");

            await ctx.RespondAsync($"{howMany} images from our {flavor} collection, just for you, Mr. {ctx.User.Mention}: " + response);
        }

        async Task<string> BuildReply(ulong channel, CommandContext ctx, int howMany)
        {
            Console.WriteLine("Building reply...\nGetting channel..");

            string reply = "";

            DiscordChannel c = await ctx.Client.GetChannelAsync(channel);

            Console.WriteLine("Got channel.\nGetting most recent message.");

            DiscordMessage mostRecent = await c.GetMessageAsync(c.LastMessageId); //c.GetMessageAsync(ctx.Channel.LastMessageId);
            
            Console.WriteLine("Got most recent message.\nSearching for messages...");
            
            List<ulong> msgIDs = new List<ulong>();

            ulong anchor = mostRecent.Id;

            Console.WriteLine($"Fetching messages (initial)... from { mostRecent.Id }");

            List<DiscordMessage> msgs = new List<DiscordMessage>();
            msgs.AddRange(await c.GetMessagesAsync(limit: 100, before: anchor));

            Console.WriteLine("Fetched.");

            int lastcount = msgs.Count;
            int allowedCycles = 50;
            int cycle = 0;

            while (lastcount == 100 && cycle<allowedCycles)
            {
                Console.WriteLine($"more messages! cycling again. at: { msgs.Count }");
                anchor = msgs.Last().Id;
                msgs.AddRange(await c.GetMessagesAsync(before: anchor));
                lastcount = msgs.Count - (100 * (cycle+1));

                cycle++;
            }

            Random r = new Random(DateTime.Now.Millisecond);

            int obtained = 0;

            while (obtained < howMany)
            {
                DiscordMessage m = msgs.ElementAt(r.Next(0, msgs.Count));

                if (m.Attachments.Count > 0)
                {
                    reply += m.Attachments.ElementAt(r.Next(0,m.Attachments.Count)).Url + " ";
                    obtained++;
                }
                else if (m.Embeds.Count > 0)
                {
                    reply += m.Embeds.ElementAt(r.Next(0, m.Embeds.Count)).Url + " ";
                    obtained++;
                }
            }
            return reply;


            /*
            Console.WriteLine("found " + msgs.Count + " posts");

            int retry = 10;

            while (retry > 0)
            {
                DiscordMessage m = msgs.ElementAt(r.Next(1, msgs.Count));
                if (m.Attachments.Count > 0)
                {
                    foreach (DiscordAttachment a in m.Attachments)
                    {
                        reply += a.Url + " ";
                    }
                    retry = 0;
                }
                else if (m.Embeds.Count > 0)
                {
                    foreach (DiscordEmbed emb in m.Embeds)
                    {
                        reply += emb.Url + " ";
                    }
                    retry = 0;
                }

                retry--;
            }

            if (reply == "")
            {
                reply = "HUB BUB BLUH.";
            }

            Console.WriteLine("Posting " + reply);
            */

        }
    }

    

    enum Channel : ulong
    {
        Cute = 420488828453781504,
        Straight = 391489418818617346,
        Gay = 396568040537325569,
        Dickgirl = 420484979567362048,
        Tf = 396578695587823616,
        UStraight = 420484500724908032,
        UGay = 420484771953770496,
        Test = 424094550542254081
    }
}
