﻿using System;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Net.WebSocket;
using DSharpPlus.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubClubBot
{
    class Program
    {
        static DiscordClient discord;
        static CommandsNextModule commands;

        static void Main(string[] args)
        {
            MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            discord = new DiscordClient(new DiscordConfiguration
            {
                Token = System.IO.File.ReadAllText("token.txt"),
                TokenType = TokenType.Bot,
                UseInternalLogHandler = true,
                LogLevel = LogLevel.Debug
            });
            /*
            discord.MessageCreated += async e =>
            {
                if (e.Message.Content.ToLower().StartsWith("!gib "))
                    await e.Message.RespondAsync(BuildReply(e.Message.Content.ToLower(), e));
                        // so, 1 = most recent message. :clap:
                Console.WriteLine("owo");
            };*/

            commands = discord.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefix = "!gib "
            });

            commands.RegisterCommands<MyCommands>();
            
            discord.SetWebSocketClient<WebSocket4NetClient>();

            Console.WriteLine("tryna connect");
            await discord.ConnectAsync();
            Console.WriteLine("connected");

            await discord.EditCurrentUserAsync(username: "Hub Club Bot");

            await Task.Delay(-1);
        }
        /*
        public async Task Gib(CommandContext ctx)
        {

        }
        static string BuildReply(string input, DSharpPlus.EventArgs.MessageCreateEventArgs e)
        {
            string reply = "";

            Channel channel;

            switch (input)
            {
                case "!gib straight":
                    channel = Channel.Straight;
                    break;
                case "!gib gay":
                    channel = Channel.Gay;
                    break;
                case "!gib cute":
                    channel = Channel.Cute;
                    break;
                case "!gib tf":
                    channel = Channel.Tf;
                    break;
                case "!gib dickgirl":
                    channel = Channel.Dickgirl;
                    break;
                case "!gib ugay":
                    channel = Channel.UGay;
                    break;
                case "!gib ustraight":
                    channel = Channel.UStraight;
                    break;
                default:
                    return "Sorry! I don't serve that flavor of filth.";
            }

            Console.WriteLine("Serving " + input + "...");

            DiscordChannel c = discord.GetChannelAsync((ulong)channel).GetAwaiter().GetResult();

            DiscordMessage mostRecent = c.GetMessageAsync(c.LastMessageId).GetAwaiter().GetResult();

            Console.WriteLine("Searching for messages...");

            IReadOnlyList<DiscordMessage> msgs = (c.GetMessagesAsync(limit: 500, before: c.LastMessageId)).GetAwaiter().GetResult();

            Console.WriteLine("found " + msgs.Count + " posts");

            Random r = new Random(DateTime.Now.Millisecond);
            int retry = 10;

            while (retry > 0)
            {
                DiscordMessage m = msgs.ElementAt(r.Next(1, msgs.Count));
                if (m.Attachments.Count > 0)
                {
                    foreach (DiscordAttachment a in m.Attachments)
                    {
                        reply += a.Url + " ";
                    }
                    retry = 0;
                }
                else if (m.Embeds.Count > 0)
                {
                    foreach (DiscordEmbed emb in m.Embeds)
                    {
                        reply += emb.Url + " ";
                    }
                    retry = 0;
                }

                retry--;
            }

            if (reply == "")
            {
                reply = "Bluh, sorry! I couldn't find anything.";
            }
            
            Console.WriteLine("Posting " + reply);

            return reply;
        }*/

        
    }
}
